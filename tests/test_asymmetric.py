from base64 import b64encode, b64decode
import json

import pytest
from falcon import testing
from cryptography.hazmat.backends.openssl.rsa import _RSAPrivateKey, _RSAPublicKey
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

from app.asymmetric import RSA
from app import app


@pytest.fixture
def cipher():
    '''Returns a Cipher instance with generated private and public key'''
    return RSA()

@pytest.fixture
def client():
    return testing.TestClient(app)

def test_keys(cipher):
    if not isinstance(cipher.private_key, _RSAPrivateKey):
        raise TypeError('Invalid private key!')
    if not isinstance(cipher.public_key, _RSAPublicKey):
        raise TypeError('Invalid public key!')

def test_encrypt(cipher):
   plaintext = 'plaintext'
   encrypted = cipher.encrypt_text(plaintext)
   decrypted = cipher.decrypt_text(encrypted).decode('ascii')

   assert plaintext == decrypted

def test_encoding(cipher):
    plaintext = 'texť'
    encrypted = b64encode(cipher.encrypt_text(plaintext)).decode('ascii')
    decrypted = cipher.decrypt_text(b64decode(encrypted)).decode('utf-8')

    assert plaintext == decrypted

