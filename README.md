# README

Please create a simple web server (framework of your choice) with 2 REST endpoints.

1. to encrypt string message using a public key
2. to decrypt cipher using a private key

You can use any libraries and any asymmetric algorithm (or more). Please write the code in a way to demonstrate your knowledge of OOP and best practices when creating a REST API. Don't forget to include requirements file, readme file and config file (if applicable). The more configurable the result will be, the better. Feel free to exceed these requirements.

### Setup

1. Using docker cli:

- build docker image `docker build .`
- start container `docker run <image_name>`

2. Using docker-compose:

- build and start `docker-compose up --build`

### Api docs

#### GET /v1/encrypted

Request: {
"text": "<text_to_be_encrypted>"
}

Response:
200 OK
{
"encrypted": "<encrypted_text>"
}

#### GET /v1/decrypted

Request: {
"text": "<text_to_be_decrypted>"
}  
Response:
200 OK
{
"decrypted": "<decrypted_text"
}
