import logging
import json
from base64 import b64encode, b64decode

import falcon

try:
    from asymmetric import RSA
except ModuleNotFoundError:
    from app.asymmetric import RSA


class EncryptionResource:

    def on_get(self, req, resp):
        resp.set_header('Accept', 'application/json')
        try:
            encrypted = cipher.encrypt_text(req.media['text'])
        except falcon.MediaNotFoundError as e:
            logging.info(f'Could not encrypt! {e}')
            raise falcon.HTTPBadRequest(
                title="Missing text to encrypt!",
                description="json format: {'text': 'text to encrypt'}"
            )

        resp.status = falcon.HTTP_200
        resp.text = json.dumps({
            "encrypted": b64encode(encrypted).decode('ascii')
        })

class DecryptionResource:

    def on_get(self, req, resp):
        resp.set_header('Accept', 'application/json')
        try:
            decrypted = cipher.decrypt_text(b64decode(req.media['text']))
        except Exception as e:
            logging.info(f'Could not decrypt! {e}')
            raise falcon.HTTPBadRequest(
                title="Incorrect text to decrypt!",
                description="Please, provide the whole text encrypted with "
                "correct public key! json format: {'text': '<ciphered>'}"
            )

        resp.status = falcon.HTTP_200
        resp.text = json.dumps({
                "decrypted": decrypted.decode('utf-8')
        })


logging.basicConfig(level=logging.INFO)

cipher = RSA()

app = falcon.App()
app.add_route('/v1/encrypted', EncryptionResource())
app.add_route('/v1/decrypted', DecryptionResource())
