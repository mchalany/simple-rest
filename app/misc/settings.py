import os


RSA_PUB_KEY = os.environ.get('RSA_PUB_KEY', None)
RSA_PRIVATE_KEY = os.environ.get('RSA_PRIVATE_KEY', None)

