import logging

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization

try:
    from misc import settings as settings
except ModuleNotFoundError:
    from app.misc import settings as settings


class RSA:

    def __init__(self, private_key=None, public_key=None):
        self._private_key = private_key
        self._public_key = public_key

    @property
    def private_key(self):
        if not self._private_key:
            path = settings.RSA_PRIVATE_KEY or 'app/misc/keys/private_key.pem'
            with open(path, 'rb') as private_key:
                self._private_key = serialization.load_pem_private_key(
                    private_key.read(),
                    password=None,
                    backend=default_backend()
                )
        return self._private_key

    @property
    def public_key(self):
        if not self._public_key:
            path = settings.RSA_PUB_KEY or 'app/misc/keys/pub_key.pem'
            with open(path, 'rb') as pub_key:
                self._public_key = serialization.load_pem_public_key(
                    pub_key.read(),
                    backend=default_backend()
                )
        return self._public_key

    def encrypt_text(self, text):
        return self.public_key.encrypt(
            bytes(text, 'utf-8'),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )

    def decrypt_text(self, cipher):
        return self.private_key.decrypt(
            cipher,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )

