FROM python:3.7-alpine

COPY app/ /simple-rest/app
WORKDIR /simple-rest/app

RUN pip install --upgrade pip

RUN apk add libffi-dev gcc python3-dev musl-dev libxml2-dev libxslt-dev openssl-dev cargo

RUN pip3 install -r requirements.txt

EXPOSE 8000
CMD ["gunicorn", "app:app", "--workers=4", "--reload", "--bind", "0.0.0.0:8000"]
